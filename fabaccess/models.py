from django.db import models
from django.conf import settings

# Create your models here.

class Userdata(models.Model):
    username=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    doliid=models.IntegerField(max_length=50)
    tagbadge=models.CharField(max_length=50)
    type=models.CharField(max_length=50, default='user')
    message=models.CharField(max_length=250, default='none')
    online=models.IntegerField(max_length=1, default=0)

class Machine(models.Model):
    nom=models.CharField(max_length=100)
    macboite=models.CharField(max_length=250)
    description=models.CharField(max_length=250)
    operationel=models.IntegerField(max_length=1, default=1)
    libre=models.IntegerField(max_length=1, default=1)
    type=models.CharField(max_length=250)

class RegistreEntree(models.Model):
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    debut=models.DateField()
    fin=models.DateField()

class RegistreMachine(models.Model):
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    debut=models.DateField()
    fin=models.DateField(null=True)

class Panne(models.Model):
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    debut=models.DateField()
    fin=models.DateField(null=True)
    cause=models.CharField(max_length=250)

class Droit(models.Model):
    machineId=models.ForeignKey(Machine, on_delete=models.CASCADE)
    username=models.ForeignKey(Userdata, on_delete=models.CASCADE)
    typeAccess=models.IntegerField(max_length=1)