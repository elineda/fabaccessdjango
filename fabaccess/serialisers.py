from rest_framework import serializers, viewsets
from .models import Userdata, Machine
from django.contrib.auth.models import User



class UserSerialiser(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= User
        fields = ('url', 'username', 'email', 'groups', 'is_superuser')

class UserdataSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Userdata
        fields  = ('username', "doliid", 'tagbadge', 'type', "message", "online")

class MachineSerialiser(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Machine
        fields = ('nom', 'macboite', 'description', 'type')



