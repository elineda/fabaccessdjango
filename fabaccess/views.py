from django.shortcuts import render
from rest_framework import serializers, viewsets
from .models import Userdata, Machine, RegistreMachine
from .serialisers import UserdataSerialiser, MachineSerialiser, UserSerialiser
from django.contrib.auth.models import User
from rest_framework import permissions
from .permissions import IsAdmin
from django.views import View
import datetime
import time
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.http import HttpResponse, JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerialiser
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser)

class UserdataViewSet(viewsets.ModelViewSet):

    queryset = Userdata.objects.all()
    serializer_class =  UserdataSerialiser
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class MachineViewSet(viewsets.ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerialiser


# route pour le node mcu

class Debutmachine(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now = datetime.datetime.now()
        timestamp = int(time.time())
        machine= Machine.objects.get(macboite=rep['idmachine'])
        if (machine.libre==0):
            return JsonResponse({'autorisation':1, 'message': 'machine deja occupée'})
        elif (machine.operationel==0):
            return JsonResponse({'autorisation':1, 'message':'Machine en panne'})
        else:
            util= Userdata.objects.get(tagbadge=rep['idbadge'])
            droi= util.droit_set.get(machineId=machine.id)
            if (droi.typeAccess==0):
                registre=RegistreMachine(machineId=machine, username=util, debut=now)
                registre.save()
                machine.libre=0
                machine.save()
                return JsonResponse({'autorisation':1,'message':'trop novice'})
            elif (droi.typeAccess==1):
                registre=RegistreMachine(machineId=machine, username=util, debut=now)
                registre.save()
                machine.libre=0
                machine.save()
                return JsonResponse({'autorisation':2,'message': "avec de l'aide"})
            else:
                registre=RegistreMachine(machineId=machine, username=util, debut=now)
                registre.save()
                machine.libre=0
                machine.save()
                return JsonResponse({"autorisation":4, 'message': 'goooo'})

class Finmachine(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        now=datetime.datetime.now()
        machine= Machine.objects.get(macboite=rep['idmachine'])
        util= Userdata.objects.get(tagbadge=rep['idbadge'])
        if (machine.libre==1):
            return HttpResponse(402)
        else:
            registre=RegistreMachine.objects.all().filter(machineId=machine, username=util).last()
            registre.fin=now
            registre.save()
            machine.libre=1
            machine.save()
            return HttpResponse(200)

class Initmachine(View):
    @csrf_exempt
    def post(self, request):
        rep=json.load(request)
        machine= Machine.objects.get(macboite=rep['idmachine'])
        if (machine.operationel==1):
            machine.libre=1
            machine.save()
            return JsonResponse({"nom": machine.nom, "type":machine.type})
        else:
            return HttpResponse(400)






