from django.contrib import admin

# Register your models here.
from .models import Userdata, Machine , RegistreEntree, RegistreMachine, Panne, Droit

admin.site.register(Userdata)
admin.site.register(Machine)
admin.site.register(RegistreEntree)
admin.site.register(RegistreMachine)
admin.site.register(Panne)
admin.site.register(Droit)