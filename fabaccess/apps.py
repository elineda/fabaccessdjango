from django.apps import AppConfig


class FabaccessConfig(AppConfig):
    name = 'fabaccess'
