"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from fabaccess import views
from django.views.decorators.csrf import csrf_exempt

router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'userdata', views.UserdataViewSet)
router.register(r'machine', views.MachineViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
    path('api/', include('rest_framework.urls', namespace='rest_framework')),
    path(r'machine/init',csrf_exempt(views.Debutmachine.as_view()), name='deb'),# mac ->nom type
    path(r'machine/debut', csrf_exempt(views.Debutmachine.as_view()), name='deb'),# bage mac- autorisation message
    path(r'machine/fin', csrf_exempt(views.Finmachine.as_view()), name='fin')#bage mac- autorisation message
]

urlpatterns += [
    path('api/', include('rest_framework.urls')),
]
